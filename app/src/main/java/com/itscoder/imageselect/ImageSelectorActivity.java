package com.itscoder.imageselect;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import com.itscoder.imageselect.model.ImageFile;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ImageSelectorActivity extends AppCompatActivity implements ImageSelectorFragment.Callback {
    public static final int DEFAULT_MAX_COUNT = 9;

    //@Arg
    boolean isSingleSelect;
    //@Arg int mMaxSelectCount = DEFAULT_MAX_COUNT;
    //@Arg boolean isShowCamera;
    //@Arg @Required(false) ArrayList<ImageFile> mSelectedPicPathList;
    ///*
    //是否是直接选择图片发送动态，目前只从动态首页跳转过来
    // */
    //@Arg @Required(false) boolean isSelectForPost = false;

    private ArrayList<ImageFile> mSelectedImageList = new ArrayList<>();
    private MenuItem mFinishMenu;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_selector);
        setView();
    }

    protected void setView() {
        Fragment fragment = new ImageSelectorFragment();
        ////setTitle(R.string.select_picture);
        //Fragment fragment = Bundler.imageSelectorFragment(isSingleSelect, mMaxSelectCount, isShowCamera)
        //    .mAlreadySelectedPicList(mSelectedPicPathList)
        //    .create();
        getSupportFragmentManager().beginTransaction().add(R.id.image_grid, fragment).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_image_select, menu);
        mFinishMenu = menu.findItem(R.id.action_finish);
        setSelectStatus();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_finish:
                postResultEvent();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSingleImageSelected(ImageFile ImageFile) {
        mSelectedImageList.add(ImageFile);
        postResultEvent();
    }

    @Override
    public void onImageSelected(ImageFile ImageFile) {
        if (!mSelectedImageList.contains(ImageFile)) {
            mSelectedImageList.add(ImageFile);
        }
        setSelectStatus();
    }

    @Override
    public void onImageUnselected(ImageFile imageFile) {
        if (mSelectedImageList.contains(imageFile)) {
            mSelectedImageList.remove(imageFile);
            setSelectStatus();
        }
    }

    @Override
    public void onImageSelectChanged(ArrayList<ImageFile> imageFiles) {
        mSelectedImageList.clear();
        mSelectedImageList.addAll(imageFiles);
        setSelectStatus();
    }

    private void setSelectStatus() {
        if (isSingleSelect) {
            mFinishMenu.setVisible(false);
        } else {
            mFinishMenu.setVisible(true);
            mFinishMenu.setEnabled(mSelectedImageList.size() > 0);
        }
    }

    @Override
    public void onCameraShot(File file) {
        if (file != null) {
            mSelectedImageList.add(new ImageFile(file.getAbsolutePath(), file.getName(), file.lastModified(), file.length()));
            postResultEvent();
        }
    }

    @Override
    public void onImageSelectFinish(List<ImageFile> imageFiles) {
        //if (isSelectForPost) {
        //    gotoSendPostPage(imageFiles);
        //} else {
        //    MDEventBus.getBus().post(new ImageSelectResultEvent(imageFiles, mClass, mId));
        //}
        finish();
    }

    private void gotoSendPostPage(List<ImageFile> imageFiles) {
        //Bundler.newSendPostActivity(NewSendPostActivity.SendType.SEND_POST, null, null, PostActivity.class)
        //    .mSelectedImageFileList((ArrayList<ImageFile>) imageFiles)
        //    .start(this);
    }

    private void postResultEvent() {
        //if (isSelectForPost) {
        //    gotoSendPostPage(mSelectedImageList);
        //} else {
        //    MDEventBus.getBus().post(new ImageSelectResultEvent(mSelectedImageList, mClass, mId));
        //}
        finish();
    }
}

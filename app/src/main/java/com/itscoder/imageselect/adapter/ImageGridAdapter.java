package com.itscoder.imageselect.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.itscoder.imageselect.R;
import com.itscoder.imageselect.model.ImageFile;
import java.util.ArrayList;
import java.util.List;

public class ImageGridAdapter extends BaseAdapter {

    private static final int TYPE_CAMERA = 0;
    private static final int TYPE_NORMAL = 1;

    private Context mContext;
    private LayoutInflater mInflater;
    private boolean showCamera = true;
    private boolean showSelectIndicator = true;

    private List<ImageFile> mImages = new ArrayList<>();
    private List<ImageFile> mSelectedImages = new ArrayList<>();
    private OnCheckClickListener mOnCheckClickListener;

    public ImageGridAdapter(Context context, boolean showCamera) {
        mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.showCamera = showCamera;
        setImageWidth();
    }

    private void setImageWidth() {
    }

    public void setOnCheckClickListener(OnCheckClickListener onCheckClickListener) {
        mOnCheckClickListener = onCheckClickListener;
    }

    /**
     * 显示选择指示器
     */
    public void showSelectIndicator(boolean b) {
        showSelectIndicator = b;
    }

    public boolean isShowCamera() {
        return showCamera;
    }

    public void setShowCamera(boolean b) {
        if (showCamera == b) return;

        showCamera = b;
        notifyDataSetChanged();
    }

    /**
     * 选择某个图片，改变选择状态
     */
    public void select(ImageFile image) {
        if (mSelectedImages.contains(image)) {
            mSelectedImages.remove(image);
        } else {
            mSelectedImages.add(image);
        }
        notifyDataSetChanged();
    }

    /**
     * 通过图片路径设置默认选择
     */
    public void setDefaultSelected(ArrayList<ImageFile> resultList) {
        mSelectedImages.clear();
        for (ImageFile image : resultList) {
            if (image != null) {
                mSelectedImages.add(image);
            }
        }
        notifyDataSetChanged();
    }

    private ImageFile getImageByPath(String path) {
        if (mImages != null && mImages.size() > 0) {
            for (ImageFile image : mImages) {
                if (image.path.equalsIgnoreCase(path)) {
                    return image;
                }
            }
        }
        return null;
    }

    public List<ImageFile> getData() {
        return mImages;
    }

    /**
     * 设置数据集
     */
    public void setData(List<ImageFile> images) {
        mSelectedImages.clear();

        if (images != null && images.size() > 0) {
            mImages = images;
        } else {
            mImages.clear();
        }
        notifyDataSetChanged();
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        if (showCamera) {
            return position == 0 ? TYPE_CAMERA : TYPE_NORMAL;
        }
        return TYPE_NORMAL;
    }

    @Override
    public int getCount() {
        return showCamera ? mImages.size() + 1 : mImages.size();
    }

    @Override
    public ImageFile getItem(int i) {
        if (showCamera) {
            if (i == 0) {
                return null;
            }
            return mImages.get(i - 1);
        } else {
            return mImages.get(i);
        }
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        int type = getItemViewType(i);
        if (type == TYPE_CAMERA) {
            view = mInflater.inflate(R.layout.list_item_camera, viewGroup, false);
            view.setTag(null);
        } else if (type == TYPE_NORMAL) {
            ViewHolder holder;
            if (view == null) {
                view = mInflater.inflate(R.layout.list_item_image, viewGroup, false);
                holder = new ViewHolder(view);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }
            if (holder != null) {
                holder.bindData(i);
            }
        }
        return view;
    }

    public interface OnCheckClickListener {
        void onCheckClick(int position);
    }

    class ViewHolder {
        ImageView image;
        ImageView ivCheckBox;
        View mask;

        ViewHolder(View view) {
            image = (ImageView) view.findViewById(R.id.image);
            ivCheckBox = (ImageView) view.findViewById(R.id.checkmark);
            mask = view.findViewById(R.id.mask);
        }

        void bindData(final int position) {
            ImageFile data = getItem(position);
            if (data == null) return;
            // 处理单选和多选状态
            if (showSelectIndicator) {
                ivCheckBox.setVisibility(View.VISIBLE);
                if (mSelectedImages.contains(data)) {
                    // 设置选中状态
                    ivCheckBox.setImageResource(R.drawable.ic_selected);
                    mask.setVisibility(View.VISIBLE);
                } else {
                    // 未选择
                    ivCheckBox.setImageResource(R.drawable.ic_unselected);
                    mask.setVisibility(View.GONE);
                }
            } else {
                ivCheckBox.setVisibility(View.GONE);
            }

            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnCheckClickListener != null) {
                        mOnCheckClickListener.onCheckClick(position);
                    }
                }
            });
            //RxViewUtil.clicks(ivCheckBox).subscribe(new Action1<Void>() {
            //    @Override
            //    public void call(Void aVoid) {
            //        if (mOnCheckClickListener != null) {
            //            mOnCheckClickListener.onCheckClick(position);
            //        }
            //    }
            //});

            // 显示图片
            String filePath = "file://" + data.path;
            Glide.with(mContext).load(filePath)
                //.asBitmap().placeholder(R.color.Gray)
                .into(image);
        }
    }
}

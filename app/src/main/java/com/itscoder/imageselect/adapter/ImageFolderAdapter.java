package com.itscoder.imageselect.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.itscoder.imageselect.R;
import com.itscoder.imageselect.model.ImageFolder;
import java.util.ArrayList;
import java.util.List;

public class ImageFolderAdapter extends BaseAdapter {

    int mImageSize;
    int lastSelected = 0;
    private Context mContext;
    private LayoutInflater mInflater;
    private List<ImageFolder> mImageFolders = new ArrayList<>();

    public ImageFolderAdapter(Context context) {
        mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mImageSize = 30;
        //mImageSize = mContext.getResources().getDimensionPixelOffset(R.dimen.folder_cover_size);
    }

    /**
     * 设置数据集
     */
    public void setData(List<ImageFolder> imageFolders) {
        if (imageFolders != null && imageFolders.size() > 0) {
            mImageFolders = imageFolders;
        } else {
            mImageFolders.clear();
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mImageFolders.size() + 1;
    }

    @Override
    public ImageFolder getItem(int i) {
        if (i == 0) return null;
        return mImageFolders.get(i - 1);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = mInflater.inflate(R.layout.list_item_folder, viewGroup, false);
            holder = new ViewHolder(view);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        if (holder != null) {
            if (i == 0) {
                holder.name.setText("所有图片");
                holder.size.setText(getTotalImageSize() + "张");
                if (mImageFolders.size() > 0) {
                    ImageFolder f = mImageFolders.get(0);
                    String folderCoverPath = "file://" + f.cover.path;
                    Glide.with(mContext).load(folderCoverPath).into(holder.cover);
                    //ImageLoader.getInstance().displayImage(folderCoverPath, holder.cover, App.DISPLAY_OPTIONS_PHOTO);
                }
            } else {
                holder.bindData(getItem(i));
            }
            if (lastSelected == i) {
                holder.indicator.setVisibility(View.VISIBLE);
            } else {
                holder.indicator.setVisibility(View.INVISIBLE);
            }
        }
        return view;
    }

    private int getTotalImageSize() {
        int result = 0;
        if (mImageFolders != null && mImageFolders.size() > 0) {
            for (ImageFolder f : mImageFolders) {
                result += f.images.size();
            }
        }
        return result;
    }

    public int getSelectIndex() {
        return lastSelected;
    }

    public void setSelectIndex(int i) {
        if (lastSelected == i) return;

        lastSelected = i;
        notifyDataSetChanged();
    }

    class ViewHolder {
        ImageView cover;
        TextView name;
        TextView size;
        ImageView indicator;

        ViewHolder(View view) {
            cover = (ImageView) view.findViewById(R.id.cover);
            name = (TextView) view.findViewById(R.id.name);
            size = (TextView) view.findViewById(R.id.size);
            indicator = (ImageView) view.findViewById(R.id.indicator);
            view.setTag(this);
        }

        @SuppressLint("SetTextI18n")
        void bindData(ImageFolder data) {
            name.setText(data.name);
            size.setText(data.images.size() + "张");
            String folderCoverPath = "file://" + data.cover.path;
            Glide.with(mContext).load(folderCoverPath).into(cover);
            //ImageLoader.getInstance().displayImage("file://" + data.cover.path, cover, App.DISPLAY_OPTIONS_PHOTO);
        }
    }
}

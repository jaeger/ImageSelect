package com.itscoder.imageselect.model;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.File;

public class ImageFile implements Parcelable {
    public String path;
    public String name;
    public long time;
    public boolean isOriginFile;
    public long size;

    public ImageFile(File file) {
        this(file.getAbsolutePath(), file.getName(), file.lastModified(), file.length());
    }

    public ImageFile(String path, String name, long time, long size) {
        this(path, name, time, size, false);
    }

    public ImageFile(String path, String name, long time, long size, boolean isOriginFile) {
        this.path = path;
        this.name = name;
        this.time = time;
        this.size = size;
        this.isOriginFile = isOriginFile;
    }

    @Override
    public boolean equals(Object o) {
        try {
            ImageFile other = (ImageFile) o;
            return this.path.equalsIgnoreCase(other.path);
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
        return super.equals(o);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.path);
        dest.writeString(this.name);
        dest.writeLong(this.time);
        dest.writeByte(this.isOriginFile ? (byte) 1 : (byte) 0);
        dest.writeLong(this.size);
    }

    protected ImageFile(Parcel in) {
        this.path = in.readString();
        this.name = in.readString();
        this.time = in.readLong();
        this.isOriginFile = in.readByte() != 0;
        this.size = in.readLong();
    }

    public static final Parcelable.Creator<ImageFile> CREATOR = new Parcelable.Creator<ImageFile>() {
        @Override
        public ImageFile createFromParcel(Parcel source) {
            return new ImageFile(source);
        }

        @Override
        public ImageFile[] newArray(int size) {
            return new ImageFile[size];
        }
    };
}
